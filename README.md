# HookDemo

#### 介绍
java hook技术的demo，入门级

#### hook的定义
hook，钩子。勾住系统中的程序逻辑，在某段SDK逻辑执行的过程中，通过代码手段拦截执行的逻辑，加入自己的逻辑。

#### 实用价值
hook是安卓面向切面（AOP)编程的基础，可以让我们在**不改变原有业务的前提**下，插入**额外的逻辑**，这种方式，既保护了原有业务的完整性，又能让额外的代码逻辑与原有的业务进行耦合。

#### 所需的知识点
* java反射 需要掌握类Class、方法Method、成员Field的使用方法。
* 阅读源码的能力



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### hook通用的思路
"偷梁换柱"是hook技术的核心思想，一共可以分为4步
* step1：确定要hook的对象
* step2：寻找要hook对象的持有者，举例如小明有一把钥匙，当钥匙是要hook的对象的时候，小明就是hook对象的持有者。
* step3：创建hook对象的动态代理，并创建该类的对象
* step4：使用创建出来的代理对象替换被hook的对象。


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
